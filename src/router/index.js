import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Blog from '../pages/Blog'
import AllBlogs from '../pages/AllBlogs'
import NewPost from '../pages/NewPost'
import EditPost from '../pages/EditPost'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Homepage'
    }
  },
  {
    path: '/newPost',
    name: 'NewPost',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: NewPost,
    meta: {
      title: 'New Post'
    }
  },
  {
    path: '/blog/:id',
    name: 'Blog',
    component: Blog,
    meta: {
      title: 'Blog'
    }
  },
  {
    path: '/blogs',
    name: 'AllBlogs',
    component: AllBlogs,
    meta: {
      title: 'All Blogs'
    }
  },
  {
    path: '/edit/:id',
    name: 'EditPost',
    component: EditPost,
    meta: {
      title: 'Edit Post'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeResolve((to, from, next) => {
  next()
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
