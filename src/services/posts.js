import https from '@/services/index'

// Define end point
const END_POINT = '/posts'

const getAllPostsService = function () {
    return https.get(END_POINT)
}

const getPost = function (post_id) {
    return https.get(END_POINT + '/' + post_id)
}

const createPost = function (title, body, userId) {
    return https.post(END_POINT, {title, body, userId})
}

const updatePost = function (postId, title, body, userId) {
    return https.put(END_POINT + '/' + postId, {postId, title, body, userId})
}

const deletePost = function (postId) {
    return https.delete(END_POINT + '/' + postId)
}

const getUserPosts = function (userId) {
    return https.get(END_POINT + '/?userId' + userId)
}

const getPostComment = function (postId) {
    return https.get(END_POINT + '/' + postId + '/comments')
}

export {
    getAllPostsService,
    getPost,
    createPost,
    updatePost,
    deletePost,
    getUserPosts,
    getPostComment
}