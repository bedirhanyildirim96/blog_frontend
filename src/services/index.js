import axios from 'axios'

/* Create axios object */
const https = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com',
    headers: {
        'Content-Type': 'application/json; charset=UTF-8'
    }
})

export default https