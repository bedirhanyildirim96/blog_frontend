import Vue from 'vue'
import Vuex from 'vuex'

import { getAllPostsService } from '@/services/posts'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts: [],
    ui: {
        error: ''
    }
  },
  getters: {
    getAllPosts(state) {
      return state.posts
    }
  },
  mutations: {
    setPosts (state, posts) {
      state.posts = posts
    },
    setErrorMessage (state, { error }) {
      state.ui.error = error
    }
  },
  actions: {
    // Get all posts
    async fetchPosts({ commit }) {
      let response = {}
      try {
        response = await getAllPostsService()
        //getAllPosts().then(result => {
        //  commit('setPosts', result.data)
        //})
      } catch (error) {
        commit('setErrorMessage', { error })
        return
      }
      commit('setPosts', response.data)
    },
    // Set ui error message
    setErrorMessage({ commit }, { error }) {
      commit('setErrorMessage', { error })
    }
  }
})
